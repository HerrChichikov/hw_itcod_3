day = "19"
month = "Апрель"
temperature = -15

print(f"Сегодня {day} {month}. На улице {temperature} градусов.")

if temperature < 0:
    print("Холодно, лучше остаться дома")
