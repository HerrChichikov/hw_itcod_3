while True:
    fib1 = 1
    fib2 = 1

    n = input("Введите номер числа в ряде Фибоначчи: ")
    if n.isnumeric() == False:
        print("Вы ввели некорректные данные, повторите попытку")
    else:
        n = int(n)
        i = 0

        while i < n - 2:
            fib_sum = fib1 + fib2
            fib1 = fib2
            fib2 = fib_sum
            i = i + 1

        print(f"Ваше число в ряде Фибоначчи: {fib2}")
        break