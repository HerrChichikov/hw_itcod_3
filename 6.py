while True:
    number = input("Введите любое неотрицательное целое число: ")

    if number.isnumeric() == False:
        print("Вы ввели некорректные данные, повторите попытку")
    else:
        number = int(number)
        if number == 1:
            print("Число 1 — не является ни простым, ни составным числом")
        else:
            rad_num = round(number ** 0.5)
            chek = 0

            for divider in range(2, rad_num + 1):
                if (number % divider == 0) and (divider != 1, 0):
                    print("Составное")
                    chek = 1
                    break

            if chek == 0:
                print("Простое")
            break
